@include('template.head')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- MODAL AREA -->
    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div id="lebar" class="modal-dialog modal-md">
            <div class="modal-content">
                <form  method="GET" action="{{route('task.create')}}" enctype="multipart/form-data" id="form-edit" class="form-horizontal">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add To Do List</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                </div>
                <div id="body" class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Title</label>
                                <input type="text" id="title" name="title" class="form-control input-sm" placeholder="" value="">
                        </div>

                         <div class="form-group">
                            <label class="col-sm-12 control-label">Tanggal</label>
                             <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                    <input type="text" name="tanggal" id="tanggal" class="form-control datetimepicker-input" data-target="#reservationdate" value="" />
                                <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                          </div>

                          <div class="bootstrap-timepicker">
                            <div class="form-group">
                              <label>JAM</label>

                              <div class="input-group date" id="timepicker" data-target-input="nearest">
                                <input type="text" name="jam" id="jam" class="form-control datetimepicker-input" data-target="#timepicker"/>
                                <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                                </div>
                                </div>
                              <!-- /.input group -->
                            </div>
                            <!-- /.form group -->
                          </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button id="submit" type="submit" value="save" class="btn btn-info">Save</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div id="lebar" class="modal-dialog modal-md">
            <div class="modal-content">
                <form  method="GET" action="" enctype="multipart/form-data" id="form-delete" class="form-horizontal">
                {{ csrf_field() }}
                <div class="modal-header">
                    <h5 class="modal-title-del" id="exampleModalLabelDel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span></button>
                </div>
                <div id="body" class="modal-body">
                    <div class="box-body">
                      <a>are you sure?</a>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="submit-del" type="submit" value="save" class="btn btn-info">Save</button>
                    <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!---- END OF MODAL AREA -->
  <div class="col-lg-8" style="margin: 0 auto;">


    <!-- TO DO List -->
            <br><br>
            <h1 class="text-center">Super2Do</h1>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="ion ion-clipboard mr-1"></i>
                  To Do List
                </h3>

                <!--<div class="card-tools">
                  <ul class="pagination pagination-sm">
                    <li class="page-item"><a href="#" class="page-link">&laquo;</a></li>
                    <li class="page-item"><a href="#" class="page-link">1</a></li>
                    <li class="page-item"><a href="#" class="page-link">2</a></li>
                    <li class="page-item"><a href="#" class="page-link">3</a></li>
                    <li class="page-item"><a href="#" class="page-link">&raquo;</a></li>
                  </ul>
                </div>-->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <ul class="todo-list" data-widget="todo-list">
                @if(!empty($data))
                <?php
                  $identity = 1;
                  //dd(count($data));
                ?>

                @foreach($data as $item)
                  <li>
                    <div class="row">
                      <div style="margin-top: 7px;">
                        <!--<span class="handle">
                          <i class="fas fa-ellipsis-v"></i>
                          <i class="fas fa-ellipsis-v"></i>-->
                        </span>
                        <!-- checkbox -->
                        <?php
                          $status = $item->status == 1 ? 'ACTIVE' : 'COMPLETED';
                        ?>
                        <div  class="icheck-primary d-inline ml-2">
                          @if ($status == 'ACTIVE')
                          <i class="far fa-square fa-2x"></i>
                            <!--<input type="checkbox" value="" name="todo1" id="{{$identity}}"> -->
                          @else
                            <i class="fas fa-check-square fa-2x"></i>
                            <!--<input type="checkbox" value="" name="todo1" id="{{$identity}}" checked>-->
                          @endif
                          <label for="{{$identity}}"></label>
                        </div>
                      </div>
                      <!-- todo text -->
                      <div class="col-sm-9">
                        
                        <!-- Emphasis label -->
                        @if ($status == 'ACTIVE')
                          <span class="text">{{ $item->title }}</span>
                          <small class="badge badge-primary">ACTIVE</small>
                        @else
                        <span class="text"><s>{{ $item->title }}</s></span>
                          <small class="badge badge-success">COMPLETED</small>
                        @endif
                        <br>
                        <span style="font-size: 12px;">{{ $item->date }} - {{ $item->time }}  </span>
                      </div>
                      <div class="" style="margin-top: 15px;">
                        @if ($status == 'ACTIVE')
                          <a title="Mark as Completed" href="{{route('task.setStatus',[$item->id, 2])}}" class="check"><button class="btn btn-sm btn-success"><i class="fas fa-check"></i></button></a>
                        @else
                          <a title="Mark as Active" href="{{route('task.setStatus',[$item->id, 1])}}" class="uncheck"><button class="btn btn-sm btn-warning"><i class="fas fa-undo"></i></button></a>
                        @endif
                        
                        <a title="Edit" href="#" data-id="{{ $item->id }}" data-title="{{ $item->title }}" data-tanggal="{{ $item->date }}" data-jam="{{ $item->time }}" data-toggle="modal" data-target="#myModal" class="editData"><button class="btn btn-sm btn-info"><i class="fas fa-edit"></i></button></a>
                        
                        <a title="Delete" href="#" data-toggle="modal" data-target="#delModal" data-id="{{ $item->id }}" class="delData"><button class="btn btn-sm btn-danger"><i class="far fa-trash-alt"></i></button></a>
                      </div>
                    </div>
                  </li>
                  <?php
                    $identity++;
                  ?>
                @endforeach
                @endif
                </ul>
              </div>
              <!-- /.card-body -->
              <div class="card-footer clearfix">
                <a href="{{route('task.index')}}">
                <button type="button" class="btn btn-btn btn-outline-danger float-left allData" style="margin-right: 10px;">All ({{ $total->total }})</button> 
                </a>
                <a href="{{route('task.getActive',1)}}">
                <button type="button" class="btn btn-btn btn-outline-primary float-left activeData" style="margin-right: 10px;">Active ({{ $total->active }})</button> 
                </a>
                <a href="{{route('task.getActive',2)}}">
                <button type="button" class="btn btn-btn btn-outline-success float-left completeData" style="margin-right: 10px;">Completed ({{ $total->completed }})</button> 
                </a>

                <button id="button-all" type="button" class="btn btn-success float-right completeAll" data-toggle="modal" data-target="#delModal"  style="margin-left:10px;"><i class="fas fa-check"></i> Mark All as Completed</button>

                <button type="button" class="btn btn-primary float-right newData" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus"></i> Add item</button>
              </div>
            </div>
  </div>
</div>
<!-- ./wrapper -->

<?php
    $base_url = url('/');
?>

@include('template.script')
<script type="text/javascript">
  var urlAction = '';
  var modal_title = '';
  var base_url = '<?php echo $base_url;?>';

   $(function () {

       let url = $(location).attr('href');
       let urlKey = url.replace(/\/\s*$/, "").split('/').pop();
       
       //console.log(urlKey);

       if (urlKey == 1) {
        $('#button-all').html('<i class="fas fa fa-check"></i>&nbsp;Set All as Completed').addClass('btn-success').removeClass('btn-danger');
          urlAction = base_url + '/completeAll';
          modal_title = 'Set All Completed';
       } else if (urlKey == 2) {
          $('#button-all').html('<i class="fas fa fa-trash"></i>&nbsp;Delete All Completed').addClass('btn-danger').removeClass('btn-success');
          urlAction = base_url + '/deleteAll';
          modal_title = 'Delete All Completed';
       } else {
          $('#button-all').hide();
       }

      $('#reservationdate').datetimepicker({
          format: 'YYYY-MM-DD'
      });

      //Timepicker
      $('#timepicker').datetimepicker({
        use24hours: true,
        format: 'HH:mm'
      })
    })

    $(document).on("click", ".editData", function () {
       var id = $(this).attr('data-id');
       var title = $(this).attr('data-title');
       var tanggal = $(this).attr('data-tanggal');
       var jam = $(this).attr('data-jam');
       //$(".modal-body #id").val(id );
       $(".modal-body #title").val( title );
       $(".modal-body #tanggal").val( tanggal );
       $(".modal-body #jam").val( jam );
        var base_url = "<?php echo $base_url;?>";
        var elems = document.querySelectorAll(".form-control");
        [].forEach.call(elems, function(el) {
            el.classList.remove("is-invalid");
        });
        $('#form-edit').validate().resetForm();

       $("#form-edit").attr('action', base_url + '/update/' + id);
       $('#submit').text('Update');
    });

    $(document).on("click", ".delData", function () {
       var id = $(this).attr('data-id');
       //$(".modal-body #id").val(id );
        var base_url = "<?php echo $base_url;?>";
       $("#form-delete").attr('action', base_url + '/delete/' + id);
       $('#submit-del').text('Delete');
    });

    $(document).on("click", ".completeAll", function () {
       var id = $(this).attr('data-id');
       //$(".modal-body #id").val(id );
        var base_url = "<?php echo $base_url;?>";
        console.log(modal_title);
       $('#exampleModalLabelDel').text(modal_title);
       $('#form-delete').attr('action', urlAction);
       $('#submit-del').text('Yes');
    });

    $(document).on("click", ".newData", function () {
       document.getElementById("form-edit").reset();
       var elems = document.querySelectorAll(".form-control");
       [].forEach.call(elems, function(el) {
            el.classList.remove("is-invalid");
        });
        $('#form-edit').validate().resetForm();
    });

    $('#form-edit').validate({
      rules: {
        title: {
          required: true
        },
        tanggal: {
          required: true
        },
        jam: {
          required: true
        }
      },
      messages: {
        title: {
          required: "Field title cannot blank.",
        },
        tanggal: {
          required: "Field tanggal cannot blank.",
        },
        jam: {
          required: "Field jam cannot blank.",
        }
      },
      errorElement: 'span',
          errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
          },
          highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
          },
          unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
          }
      });
</script>
</body>
</html>
