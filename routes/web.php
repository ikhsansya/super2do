<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [TaskController::class, 'index'])->name('task.index');
Route::get('/create', [TaskController::class, 'create'])->name('task.create');
Route::get('/update/{id}', [TaskController::class, 'update'])->name('task.update');
Route::get('/delete/{id}', [TaskController::class, 'delete'])->name('task.delete');
Route::get('/set-status/{id}/{status}', [TaskController::class, 'setStatus'])->name('task.setStatus');
Route::get('/getStatus/{status}', [TaskController::class, 'getActive'])->name('task.getActive');
Route::get('/completeAll', [TaskController::class, 'completeAll'])->name('task.completeAll');
Route::get('/deleteAll', [TaskController::class, 'deleteAll'])->name('task.deleteAll');
