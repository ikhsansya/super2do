<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;

class TaskController extends Controller
{   

    public function index() {

        $response_detail = new \stdClass();

        try {
            $client   = new \GuzzleHttp\Client();
            $response = $client->request('GET', config('global.API_URL').'/api/v1/task', [
                'verify'  => false,
            ]);

            $response_body                        = json_decode($response->getBody());
            $response_data                        = $response_body->data;
            $response_detail->body                = new \stdClass();
            $response_detail->body                = $response_data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response_detail->error = $e->getMessage();
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response_detail->error = $e->getMessage();
        } catch (\Exception $e) {
            $response_detail->error = $e->getMessage();
        }

        $data = $response_detail->body; 
        $total = $this->getTotal();
        //dd($total);      
        
        return view('super2do', compact('data','total'));
    }

    public function create(Request $request) {
         $response_detail = new \stdClass();

        try {
            $client   = new \GuzzleHttp\Client();
            $url = config('global.API_URL').'/api/v1/task';
            $form_params = [
                'title'             => $request->input('title'),
                'date'              => $request->input('tanggal'),
                'time'              => $request->input('jam'),
                'status'            => '1',
            ];

            //dd($form_params);
            $response = $client->post($url, ['form_params' => $form_params]);
            $response = $response->getBody()->getContents();
           

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response_detail->error = $e->getMessage();
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response_detail->error = $e->getMessage();
        } catch (\Exception $e) {
            $response_detail->error = $e->getMessage();
        }

         return Redirect::back();

    }

    public function update(Request $request, $id){
        $response_detail = new \stdClass();
        //dd($request->all());

        try {
            $client   = new \GuzzleHttp\Client();
            $url = config('global.API_URL').'/api/v1/task/'.$id;
            $form_params = [
                'title'             => $request->input('title'),
                'date'              => $request->input('tanggal'),
                'time'              => $request->input('jam'),
                'status'              => '1'
            ];
            $form_params = json_encode($form_params);
            //dd($form_params);

            //dd($form_params);
            $response = $client->put($url, 
                             [  'headers' => ['Content-Type' => 'application/json','Accept' => 'application/json'],
                                'body' => $form_params]
                        );
            //dd($response->getBody()->getContents());
           

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            //dd($e->getMessage());
            $response_detail->error = $e->getMessage();
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            //dd($e->getMessage());
            $response_detail->error = $e->getMessage();
        } catch (\Exception $e) {
            $response_detail->error = $e->getMessage();
        }

         return Redirect::back();
    }

    public function delete($id){
        $response_detail = new \stdClass();

        try {
            $client   = new \GuzzleHttp\Client();
            $url = config('global.API_URL').'/api/v1/task/'.$id;
            

            
            $response = $client->delete($url);
            $response = $response->getBody()->getContents();
            //dd($response);
           

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response_detail->error = $e->getMessage();
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response_detail->error = $e->getMessage();
        } catch (\Exception $e) {
            $response_detail->error = $e->getMessage();
        }

         return Redirect::back();
    }

    public function setStatus($id, $status){
       $response_detail = new \stdClass();

        try {
            $client   = new \GuzzleHttp\Client();
            $url = config('global.API_URL').'/api/v1/task/set-status/'.$id;
            
            $form_params = [
                'status'            => $status,
            ];

            //dd($form_params);

            $response = $client->post($url, ['form_params' => $form_params]);
            $response = $response->getBody()->getContents();

           

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response_detail->error = $e->getMessage();
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response_detail->error = $e->getMessage();
        } catch (\Exception $e) {
            $response_detail->error = $e->getMessage();
        }

         return Redirect::back();
    }

    public function getActive($status) {
        $response_detail = new \stdClass();

        try {
            $client   = new \GuzzleHttp\Client();
            $url = config('global.API_URL').'/api/v1/task?status='.$status;
            $response = $client->request('GET', $url, [
                'verify'  => false
            ]);

            $response_body                        = json_decode($response->getBody());
            $response_data                        = $response_body->data;
            $response_detail->body                = new \stdClass();
            $response_detail->body                = $response_data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response_detail->error = $e->getMessage();
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response_detail->error = $e->getMessage();
        } catch (\Exception $e) {
            $response_detail->error = $e->getMessage();
        }

        $data = $response_detail->body;
        $total = $this->getTotal();

        return view('super2do', compact('data','total'));
    }

    public function getTotal() {
        $response_detail = new \stdClass();

        try {
            $client   = new \GuzzleHttp\Client();
            $response = $client->request('GET', config('global.API_URL').'/api/v1/task/total', [
                'verify'  => false,
            ]);

            $response_body                        = json_decode($response->getBody());
            $response_data                        = $response_body->data;
            $response_detail->body                = new \stdClass();
            $response_detail->body                = $response_data;
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $response_detail->error = $e->getMessage();
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            $response_detail->error = $e->getMessage();
        } catch (\Exception $e) {
            $response_detail->error = $e->getMessage();
        }

        $data = $response_detail->body;       
        
        return $data;
    }

    public function completeAll() {
        $response_detail = new \stdClass();
        //dd($request->all());

        try {
            $client   = new \GuzzleHttp\Client();
            $url = config('global.API_URL').'/api/v1/task/set-complete-all';
           
            //dd($form_params);

            //dd($form_params);
            $response = $client->post($url);
            $response = $response->getBody()->getContents();
            //dd($response->getBody()->getContents());
           

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            //dd($e->getMessage());
            $response_detail->error = $e->getMessage();
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            //dd($e->getMessage());
            $response_detail->error = $e->getMessage();
        } catch (\Exception $e) {
            $response_detail->error = $e->getMessage();
        }

         return Redirect::back();
    }

    public function deleteAll() {
        $response_detail = new \stdClass();

        try {
            $client   = new \GuzzleHttp\Client();
            $url = config('global.API_URL').'/api/v1/task/delete-all-complete';
           
            $response = $client->post($url);
            $response = $response->getBody()->getContents();
            //dd($response->getBody()->getContents());
           

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            //dd($e->getMessage());
            $response_detail->error = $e->getMessage();
        } catch (\GuzzleHttp\Exception\RequestException $e) {
            //dd($e->getMessage());
            $response_detail->error = $e->getMessage();
        } catch (\Exception $e) {
            $response_detail->error = $e->getMessage();
        }

         return Redirect::back();
    }

}
